"use strict"
const Thingrator = require("generator-thing/generators/thingrator")
const randText = require("./randtext.js")

module.exports = class Godator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generation God")
      this._blackquote(
        `Receive thy new possessor—one who brings \n` +
          `A mind not to be changed by place or time. \n` +
          `The mind is its own place, and in itself \n` +
          `Can make a Heaven of Hell, a Hell of Heaven. \n`,
      )
    }
  }
  end() {
    if (!this.options.composedBy) {
      this._eliosay(`What matter where, if I be still the same \n`)
    }
  }
  prompting() {
    const prompts = [
      {
        type: "input",
        name: "pillar",
        message: "Tags going into the pillar",
        default:
          "h1, h2, h3, h4, h5, p, ul, ol, dl, table," ||
          this.config.get("pillar"),
      },
      {
        type: "input",
        name: "heaven",
        message: "Tags bound for heaven",
        default: "blockquote, h6, summary," || this.config.get("heaven"),
      },
      {
        type: "input",
        name: "hell",
        message: "Tags sent to hell",
        default: "nav, aside, details," || this.config.get("hell"),
      },
    ]
    return this.prompt(prompts).then((answer) => {
      this.answer = answer
    })
  }
  writing() {
    this.paths()
    const pkgJson = {
      devDependencies: {
        "@elioway/innocent": "^1.2.4",
        autoprefixer: "^10.4.13",
        "browser-sync": "^2.27.10",
        chai: "^4.3.7",
        "chai-dom": "^1.11.0",
        cssnano: "^5.1.14",
        gulp: "^4.0.2",
        "gulp-concat": "^2.6.1",
        "gulp-css-purge": "^3.0.9",
        "gulp-postcss": "^9.0.1",
        "gulp-rename": "^2.0.0",
        "gulp-replace": "^1.1.3",
        "gulp-sass": "^5.1.0",
        "gulp-sourcemaps": "^3.0.0",
        "gulp-uglify": "^3.0.2",
        jquery: "^3.6.1",
        jsdom: "20.0.3",
        "jsdom-browser": "^0.6.0",
        "jsdom-global": "3.0.2",
        mocha: "^10.1.0",
        "node-sass": "^8.0.0",
        prettier: "2.8.0",
        "sanitize.css": "latest",
        sass: "^1.56.1",
        "sass-true": "^6.1.0",
        "sassy-lists": "^3.0.1",
      },
      keywords: ["css", "scss", "html"],
      main: "stylesheets/theme.scss",
      scripts: {
        build: "gulp build",
        test: "mocha --recursive",
      },
    }
    // this._write("package.json") replaced
    this.fs.extendJSON(this.destinationPath("package.json"), pkgJson)
    this._copy("css/main.css")
    this._copy("css/normalize.css")
    this._copy("js/main.js")
    this._copy("js/plugins.js")
    this._copy("gulpfile.js")
    this._copy("test/suites/adonTesterSuite.js")
    this._writeGod("index.html")
    this._writeGod("README.md")
    this._writeGod("stylesheets/_pillar.scss")
    this._writeGod("stylesheets/_heaven.scss")
    this._writeGod("stylesheets/_hell.scss")
    this._writeGod("stylesheets/judge.scss")
    this._writeGod("stylesheets/settings.scss")
    this._writeGod("stylesheets/theme.scss")
    this._writeGod("stylesheets/functions/_i_am.scss")
    this._writeGod("stylesheets/mixins/_i_am.scss")
    this._writeGod("test/functions/_i_am.scss")
    this._writeGod("test/mixins/_i_am.scss")
    this._writeGod("test/stylesheets.js")
    this._writeGod("test/stylesheets.scss")
    this._beSinful(this._arrayifyCommaSeparation(this.answer.pillar), "pillar")
    this._beSinful(this._arrayifyCommaSeparation(this.answer.heaven), "heaven")
    this._beSinful(this._arrayifyCommaSeparation(this.answer.hell), "hell")
  }
  _writeGod(fileName) {
    this._write(fileName, fileName, {
      date: new Date().toISOString().split("T")[0],
      pillar: this._arrayifyCommaSeparation(this.answer.pillar),
      heaven: this._arrayifyCommaSeparation(this.answer.heaven),
      hell: this._arrayifyCommaSeparation(this.answer.hell),
      randText: randText,
    })
  }
  _beSinful(arr, position) {
    let self = this
    arr.forEach((sinner) => {
      self._sin(sinner, position)
    })
  }
  _sin(tagName, position) {
    this._write(
      "stylesheets/tagName/_" + position + ".scss",
      "stylesheets/" + tagName + "/_" + position + ".scss",
      {
        tagName: tagName,
      },
    )
  }
}
