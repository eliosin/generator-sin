require('jsdom-global')()
global.jQuery = require("jquery");
global.$ = global.jQuery;
var should = require("chai")
  .use(require("chai-dom"))
  .should();

exports.AdonTesterSuite = function(testname, scripts, html, tests) {
  describe(testname, function() {

    before(done => {
      require(scripts)
      done()
    })

    beforeEach(done => {
      document.body.innerHTML = html
      done()
    })

    tests()

    after(done => {
      done()
    })
  })
}
