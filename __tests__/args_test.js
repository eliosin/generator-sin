"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

// var deps = [[helpers.createDummyGenerator(), "thing:repo"]]

describe("generator-sin args", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/god"))
      // .withGenerators(deps)
      .inDir(path.join(__dirname, "test_generated", "args_test", "no_args"))
      .withOptions({})
      .withPrompts({
        pillar: "h1",
        heaven: "h2",
        hell: "h3",
      })
      .on("end", done)
  })
  it.skip("works with no args", () => {
    assert.fileContent(
      "README.md",
      "https://elioway.gitlab.io/args_test/no-args/installing.html",
    )
  })
})

describe("generator-sin identifier arg", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/god"))
      // .withGenerators(deps)
      .inDir(
        path.join(__dirname, "test_generated", "args_test", "identifier_arg"),
      )
      .withOptions({
        identifier: "apprentice",
      })
      .withPrompts({
        pillar: "h1",
        heaven: "h2",
        hell: "h3",
      })
      .on("end", done)
  })
  it.skip("works with identifier arg", () => {
    assert.fileContent(
      "README.md",
      "https://elioway.gitlab.io/identifier_arg/apprentice/installing.html",
    )
  })
})

describe("generator-sin subjectOf arg", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/god"))
      // .withGenerators(deps)
      .inDir(
        path.join(__dirname, "test_generated", "args_test", "subjectOf_arg"),
      )
      .withOptions({
        subjectOf: "evilWizard",
      })
      .withPrompts({
        pillar: "h1",
        heaven: "h2",
        hell: "h3",
      })
      .on("end", done)
  })
  it.skip("works with subjectOf arg", () => {
    assert.fileContent(
      "README.md",
      "https://elioway.gitlab.io/evilWizard/subject-of-arg/installing.html",
    )
  })
})

describe("generator-sin all args", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/god"))
      // .withGenerators(deps)
      .inDir(path.join(__dirname, "test_generated", "args_test", "all_args"))
      .withOptions({
        identifier: "apprentice",
        subjectOf: "evilWizard",
      })
      .withPrompts({
        pillar: "h1",
        heaven: "h2",
        hell: "h3",
      })
      .on("end", done)
  })

  it.skip("works with all args", () => {
    assert.fileContent(
      "README.md",
      "https://elioway.gitlab.io/evilWizard/apprentice/installing.html",
    )
  })
})
