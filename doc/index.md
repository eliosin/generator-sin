<aside>
  <dl>
  <dd>This God-like act</dd>
  <dd>Annuls thy doom, the death thou shouldest have died,</dd>
  <dd>In sin for ever lost from life</dd>
</dl>
</aside>

**generator-sin** is a yeoman generator for eliosin's three _patterns_ **god**, **eve** and **adon** . Used together you can rapidly create CSS wireframes and themes for prototyping or for app development **the elioWay**.

**generator-sin**'s purpose is to enshrine eliosin's principals and _patterns_ into code and be a usable app for elioWay disciples.

**generator-sin** commands will install everything you need to play **god** with **eve** and **adon** in your current folder.

# Tooling for

generator-sin is a tool for working with the follow eliosin modules.

- [eliosin/god](/eliosin/god)
- [eliosin/eve](/eliosin/eve)
- [eliosin/adon](/eliosin/adon)

# Related

- [elioangels/sassy-fibonacciness](/elioangels/sassy-fibonacciness)
- [elioangels/say](/elioangels/say)
