# Installing generator-sin

- [generator-sin Prerequisites](/eliosin/generator-sin/prerequisites.html)

- Install eliosin's yeoman generator, **generator-sin** globally.

- Install HTML5 Boilerplate yeoman generator globally.

```shell
npm install -g yo
npm install -g generator-sin
```

## Updating

```
ncu -u -t minor
```
